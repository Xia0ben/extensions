<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Export to JPEG</name>
    <id>org.inkscape.raster.jpeg_output</id>

    <param name="tab" type="notebook">
        <page name="about" gui-text="Low Quality Warning">
            <label>JPEG Output is good for photos but creates artefacts and distortions in logos and other flat vector images. Your artwork will lose visual quality and any transparency.</label>
            <image>raster_output_jpeg.svg</image>
            <label>This is a high compression example for demonstration.</label>
            <spacer/>
            <label appearance="header">You put a lot of talent, time and energy into your work.</label>
            <label>Your work deserves better so you should consider PNG or WebP file formats with lossless compression instead.</label>
            <spacer/>
            <label>Learn more details about jpeg:</label>
            <label appearance="url" indent="1">https://inkscape.org/learn/jpeg/</label>
        </page>
        <page name="options" gui-text="Options">
            <param name="quality" type="int" min="0" max="95"
                gui-text="Quality:"
                gui-description="Quality between 0 and 95"
            >75</param>
            <param name="progressive" type="bool"
                gui-text="Progressive:"
                gui-description="Store image as a progressive JPEG file."
            >true</param>
        </page>
    </param>

    <output raster="true">
        <extension>.jpeg</extension>
        <mimetype>image/jpeg</mimetype>
        <filetypename>Jpeg (*.jpeg)</filetypename>
        <filetypetooltip>Render to JPEG file format</filetypetooltip>
    </output>

    <script>
        <command location="inx" interpreter="python">raster_output_jpeg.py</command>
    </script>
</inkscape-extension>
